/**
* Name: Rushil Popat
* Class ID: 410
* Assignment Number: 2
* Description: The SimpleList class is an array of 10 integers.
* Values can be added to it, removed from it, the list can be printed,
* the number of elements can be counted, and the index of an element can be searched for.
*
* Link to git repo: https://bitbucket.org/rush1x/cse360assign2/src/master/
*/

package cse360assign2;

public class SimpleList
{
    private int[] list;
    private int count;

    /**
     * Class constructor.
     */
    public SimpleList()
    {
        list = new int[10];
        count = 0;
    }

    /**
     * Adds an integer to list. If the list is full, expand the list by 50% and
     * add.
     *
     * @param param
     *                  the integer to add to list
     */
    public void add(int param)
    {
        if (list.length == count)
        {
            int[] saveList = list.clone();
            list = new int[(int) Math.floor(saveList.length * 1.5)];
            for (int index = 0; index < saveList.length; index++)
                list[index] = saveList[index];
        }
        for (int index = list.length - 2; index >= 0; index--)
            list[index + 1] = list[index];
        count++;
        list[0] = param;
    }

    /**
     * Removes an integer from list if the list has the integer. If the list is
     * less than 75% full, decrease the size of the list
     *
     * @param param
     *                  the integer to remove from list
     */
    public void remove(int param)
    {
        int index = 0;
        boolean flag = false;
        while (index < count && !flag)
        {
            if (list[index] == param)
            {
                for (int move = index; move < count - 1; move++)
                {
                    list[move] = list[move + 1];
                }
                flag = true;
                count--;
            }
            index++;
        }
        if (count < Math.floor(list.length * .75) == true)
        {
            int[] saveList = list.clone();
            list = new int[count];
            for (int copyIndex = 0; copyIndex < list.length; copyIndex++)
                list[copyIndex] = saveList[copyIndex];
        }
    }

    /**
     * Returns the number of elements in the list
     *
     * @return the value of count
     */
    public int count()
    {
        return count;
    }

    /**
     * Returns the elements of list as a string with a single space between
     * elements
     *
     * @return list as a string
     */
    public String toString()
    {
        String listAsString = "";
        if (count > 0)
        {
            for (int index = 0; index < count - 1; index++)
            {
                listAsString = listAsString + list[index] + " ";
            }
            listAsString = listAsString + list[count - 1];
        }
        return listAsString;
    }

    /**
     * Finds the index of an integer in the list and returns it if it exists.
     * Else, it returns -1
     *
     * @param param
     *                  the integer to find
     * @return index of integer to find
     */
    public int search(int param)
    {
        int location = -1;
        for (int index = 0; index < count; index++)
        {
            if (list[index] == param && location == -1)
            {
                location = index;
            }
        }
        return location;
    }

    /**
     * Appends the parameter to the end of the list. If the list is full, then
     * increase the size by 50% so there will be room.
     *
     * @param element
     *                    the number to add to the end of the list
     *
     */
    public void append(int element)
    {
        if (list.length == count)
        {
            int[] saveList = list.clone();
            list = new int[(int) Math.floor(saveList.length * 1.5)];
            for (int index = 0; index < saveList.length; index++)
                list[index] = saveList[index];
        }
        list[count] = element;
        count++;
    }

    /**
     * Returns the first element in list. If there are no elements, return -1
     *
     * @return the first element in list
     */
    public int first()
    {
        if (count > 0)
            return list[0];
        else
            return -1;
    }

    /**
     * Returns the last element in list. If there are no elements, return -1
     *
     * @return the last element in list
     */
    public int last()
    {
        if (count > 0)
            return list[count - 1];
        else
            return -1;
    }

    /**
     * Returns the current number of possible locations in the list
     *
     * @return possible locations of the list
     */
    public int size()
    {
        return list.length;
    }
}